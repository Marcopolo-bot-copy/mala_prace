# Breaking Bad Simulator

## Popis

Breaking Bad Simulator je interaktivní simulace, která uživatelům umožňuje prožít klíčové momenty a rozhodnutí z populárního televizního seriálu "Breaking Bad". Tento projekt je napsán v Pythonu a poskytuje uživatelské rozhraní pro simulaci různých scénářů z celého seriálu.

## Funkce

- **Výběr postav**: Umožňuje hráčům vybrat si z hlavních postav seriálu.
- **Rozhodovací stromy**: Každá volba ovlivňuje další průběh příběhu.
- **Simulace důsledků**: Vizualizace důsledků vašich rozhodnutí na další vývoj příběhu.

## Instalace

Pro spuštění simulátoru je potřeba mít nainstalovaný Python 3.8 nebo novější. Následujte tyto kroky pro instalaci a spuštění projektu:

```bash
# Klonujte repozitář
git clone https://github.com/vasGithub/breaking-bad-simulator.git

# Přejděte do složky s projektem
cd breaking-bad-simulator

# Nainstalujte potřebné knihovny
pip install -r requirements.txt

# Spusťte simulátor
python simulator.py
