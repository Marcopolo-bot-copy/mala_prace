# Nápověda pro Breaking Bad Simulator

## Často kladené dotazy

**Jak spustit simulátor?**

- Otevřete příkazovou řádku nebo terminál.
- Přejděte do složky s projektem.
- Spusťte příkaz `python simulator.py`.

**Jak vybrat postavu?**

- Po spuštění simulátoru vyberte postavu z nabídky na úvodní obrazovce.

**Co dělat, pokud narazím na chybu?**

- Zaznamenejte chybu a pošlete ji na email podpory nebo přidejte do sekce Issues na GitHub stránce projektu.

**Kde mohu najít další informace?**

- Podrobnější dokumentaci a instrukce najdete v souboru `docs.md`.

## Potřebujete další pomoc?

Pokud potřebujete další pomoc nebo máte jakékoli další dotazy, neváhejte nás kontaktovat na [email@email.com](mailto:email@email.com).
