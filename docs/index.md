# Dokumentace pro Breaking Bad Simulator

## Obsah

- [Úvod](#úvod)
- [Spuštění simulátoru](#spuštění-simulátoru)
- [Možnosti uživatele](#možnosti-uživatele)
- [Struktura kódu](#struktura-kódu)
- [Přidávání nových scénářů](#přidávání-nových-scénářů)
- [FAQ](#faq)
- [Kontakt](#kontakt)

## Úvod

Breaking Bad Simulator je aplikace vytvořená v Pythonu, která uživatelům umožňuje interaktivně procházet různé scénáře založené na seriálu Breaking Bad. Cílem je poskytnout uživatelům zábavnou a edukativní zkušenost, jaké by to mohlo být rozhodovat se v kritických situacích jako postavy ze seriálu.

## Spuštění simulátoru

Simulátor lze spustit následujícími kroky:

1. Otevřete terminál.
2. Přejděte do složky s projektem pomocí příkazu `cd cesta/k/složce`.
3. Spusťte simulátor příkazem `python simulator.py`.

## Možnosti uživatele

Uživatel má následující možnosti:

- **Výběr postavy**: Vyberte si jednu z hlavních postav seriálu.
- **Rozhodnutí**: Na základě nabídnutých možností rozhodněte, jaká akce bude provedena.
- **Prohlížení důsledků**: Zobrazte výsledky vašich rozhodnutí.

## Struktura kódu

Projekt je organizován do následujících hlavních složek a souborů:

- **`simulator.py`**: Hlavní spustitelný soubor, který obsahuje logiku uživatelského rozhraní.
- **`characters.py`**: Modul pro definici postav.
- **`scenarios.py`**: Modul obsahující logiku scénářů.
- **`utils.py`**: Pomocné funkce používané napříč aplikací.

## Přidávání nových scénářů

Pro přidání nového scénáře:

1. Vytvořte novou funkci v `scenarios.py`, která definuje scénář.
2. Přidejte logiku pro volbu tohoto scénáře do `simulator.py`.
3. Ujistěte se, že jsou všechny potřebné proměnné správně propojeny.

## FAQ

**Q: Jak mohu přispět k projektu?**  
A: Přispět můžete vytvořením forku repozitáře, přidáním nových funkcí a odesláním pull requestu.

**Q: Kde mohu hlásit chyby?**  
A: Chyby můžete hlásit v sekci Issues na GitHub stránce projektu.

## Kontakt

Pro více informací nebo případné dotazy se můžete obrátit na [email@email.com](mailto:email@email.com).
